import os

def list():
    for i in os.listdir():
        print(i)

def goto():
    os.chdir(input('Goto '))

def read():
    try:
        file = open(input('Read '))
    except OSError:
        print('File Not Found')
    print(file.read())
    file.close()

def mkdir():
    os.mkdir(input('Create new folder named '))

def rmdir():
    os.rmdir(input('Remove a folder named '))

def stat():
    print(os.stat(input('Find the staus of a file/directory named ')))

print('uPyUI Files')

cmds = {'list':list, 'goto':goto, 'read':read, 'mkdir':mkdir, 'rmdir':rmdir, 'stat':stat}
while 1:
    print(os.getcwd())
    inp = input('Run ')
    if inp == 'exit':
        break
    if inp == 'programs':
        print('Command Name         UI Type    Desc')
        print('list    List         None       Lists files and folders in wd.')
        print('goto    GoTo         Shell-like Goes to specified folder.')
        print('read    Read         Shell-like Reads specified file.')
        print('mkdir   MkDir        Prompt     Makes a new directory.')
        print('rmdir   RmDir        Prompt     Removes a directory.')
        print('stat    Status       Prompt     Returns status of file/directory.')
        print('exit    Exit         None       Exits shell, enters REPL.')
    elif inp == '':
        pass
    else:
        try:
            cmds[inp]()
        except KeyError:
            print('Command Not Found')