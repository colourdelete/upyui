def wifi():
    import wifi_config

def files():
    import file_manager

def ada1120():
    # Code in this function based off of Adafruit Learning Center guide for ada1120.
    import board
    import busio
    import adafruit_lsm303
    i2c = busio.I2C(board.SCL, board.SDA)
    sensor = adafruit_lsm303.LSM303(i2c)
    return i2c, sensor

def ada1120_accel():
    # Code in this function based off of Adafruit Learning Center guide for ada1120.
    i2c, sensor = ada1120()
    print('{0:0.3f} {1:0.3f} {2:0.3f}'.format(*sensor.acceleration))

def ada1120_accel():
    # Code in this function based off of Adafruit Learning Center guide for ada1120.
    i2c, sensor = ada1120()
    print('{0:0.3f} {1:0.3f} {2:0.3f}'.format(*sensor.magnetic))

print('uPyUI Shell')
print('Choose a program to run.')
print('To see a list, type programs')
cmds = {'wifi':wifi, 'files':files, 'ada1120_accel':ada1120_accel}
while 1:
    inp = input('Run ')
    if inp == 'exit':
        break
    if inp == 'programs':
        print('Command       Name                       UI Type    Desc')
        print('wifi          Wi-Fi Config               Shell-like Configures Wi-Fi.')
        print('files         File Manager               Shell-like Browse files/folders.')
        print('ada1120_accel Adafruit 1120 Accel Output None       Serial raw outputter (accel) for plotting. (Adafruit Triple-axis Accelerometer+Magnetometer (Compass) Board - LSM303)')
    elif inp == '':
        pass
    else:
        try:
            cmds[inp]()
        except KeyError:
            print('Command Not Found')