import board
import busio
import adafruit_lsm303

print('uPyUI Ada1120 Orient')

def open():
    i2c = busio.I2C(board.SCL, board.SDA)
    return i2c, adafruit_lsm303.LSM303(i2c)

def raw(sensor):
    x, y, z = '{0:0.3f} {1:0.3f} {2:0.3f}'.format(*sensor.acceleration).split()
    return float(x), float(y), float(z)

def orient(sensor):
    x, y, z = '{0:0.3f} {1:0.3f} {2:0.3f}'.format(*sensor.acceleration).split()
    x, y, z = float(x), float(y), float(z)
    x, y, z = round(x)/10, round(y)/10, round(z)/10
    return [x*180, y*180, z*180]

def close(i2c):
    i2c.deinit()