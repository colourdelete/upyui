import time, board, pulseio
import ada1120_orient as orient

i, s  = orient.open() # i2c, sensor

freq  = 100 # 100Hz
m_a   = pulseio.PWMOut(board.D14, fequency=freq) # motor_a
m_b   = pulseio.PWMOut(board.D12, fequency=freq)
m_c   = pulseio.PWMOut(board.D13, fequency=freq)
m_d   = pulseio.PWMOut(board.D15, fequency=freq)

m_a_p = 1 # motor_a_power
m_b_p = 1
m_c_p = 1
m_d_p = 1

while 1:
    m_a_p = 1
    m_b_p = 1
    m_c_p = 1
    m_d_p = 1
    deg   = orient.orient()[2:] # goal: deg = [162, 0, 54]
    if deg[0] < 0:
        m_a_p -= 0
        m_b_p -= 0
        m_c_p -= 0
        m_d_p -= 0
    elif deg[0] > 0:
        m_a_p -= 0
        m_b_p -= 0
        m_c_p -= 0
        m_d_p -= 0
    if deg[1] < 0:
        m_a_p -= 0
        m_b_p -= 0.2
        m_c_p -= 0
        m_d_p -= 0.2
    elif deg[1] > 0:
        m_a_p -= 0.2
        m_b_p -= 0
        m_c_p -= 0.2
        m_d_p -= 0
    if deg[2] > 54:
        m_a_p -= 0.2
        m_b_p -= 0.2
        m_c_p -= 0
        m_d_p -= 0
    if deg[2] < 54:
        m_a_p -= 0
        m_b_p -= 0
        m_c_p -= 0.2
        m_d_p -= 0.2
    m_a.duty_cycle = 2 ** 16*m_a_p
    m_b.duty_cycle = 2 ** 16*m_b_p
    m_c.duty_cycle = 2 ** 16*m_c_p
    m_d.d   uty_cycle = 2 ** 16*m_d_p

orient.close(s)